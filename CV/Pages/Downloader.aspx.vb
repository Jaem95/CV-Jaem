﻿Imports System.Data
Imports System.Data.SqlClient

Partial Public Class Downloader
    Inherits System.Web.UI.Page

    Private GlobalDBConnection As String = ConfigurationManager.ConnectionStrings("DB").ConnectionString

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim guid As String = Request.QueryString("guid")

        If String.IsNullOrEmpty(guid) Then Return

        ' aquí puedes poner las validaciones que quieras 

        '............   .................... 

        Dim dt As DataTable = document_attachment_GetByGuid(guid)
        If dt.Rows.Count = 0 Then Return

        Dim OriginalFileName As String = dt.Rows(0)("filename")
        DownloadFile(Server.MapPath("./Uploads/") & guid, OriginalFileName)
    End Sub

    Public Sub DownloadFile(ByVal FilePath As String, ByVal OriginalFileName As String)
        Dim fs As IO.FileStream = Nothing

        'obtenemos el archivo del servidor 
        fs = IO.File.Open(FilePath, IO.FileMode.Open, IO.FileAccess.Read)
        Dim byteBuffer(CInt(fs.Length - 1)) As Byte
        fs.Read(byteBuffer, 0, CInt(fs.Length))
        fs.Close()

        Using ms As New IO.MemoryStream(byteBuffer)
            'descargar con su nombre original 
            Response.AddHeader("Content-Disposition", "attachment; filename=" & OriginalFileName)
            ms.WriteTo(Response.OutputStream)
        End Using

    End Sub

    Function document_attachment_GetByGuid(ByVal guid As String) As DataTable
        Dim connection As New SqlConnection(GlobalDBConnection)
        Dim command As New SqlDataAdapter("document_attachment_GetByGuid", connection)
        command.SelectCommand.CommandType = CommandType.StoredProcedure
        command.SelectCommand.Parameters.AddWithValue("@guid", guid)
        Dim dt As New DataTable()
        command.Fill(dt)

        Return dt
    End Function

End Class