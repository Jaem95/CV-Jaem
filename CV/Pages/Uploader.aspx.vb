﻿Imports System.Data
Imports System.Data.SqlClient

Public Class Uploader
    Inherits System.Web.UI.Page

    Private GlobalDBConnection As String = ConfigurationManager.ConnectionStrings("DB").ConnectionString

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsPostBack Then Return

        document_attachment_GetAll()

    End Sub

    Protected Sub btnUpload_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnUpload.Click
        If Not FileUpload1.HasFile Then Return

        Dim fileguid As String = Guid.NewGuid.ToString
        Dim FileName As String = FileUpload1.FileName

        Try
            FileUpload1.SaveAs(Server.MapPath("./Uploads/") & fileguid)
        Catch
            'error....
            Return
        End Try

        Dim i As Integer = document_attachment_Insert(FileName, fileguid)
        If i = 0 Then
            'error....
        Else
            document_attachment_GetAll()
        End If

    End Sub

    Sub document_attachment_GetAll()
        Dim connection As New SqlConnection(GlobalDBConnection)
        Dim command As New SqlDataAdapter("document_attachment_GetAll", connection)
        command.SelectCommand.CommandType = CommandType.StoredProcedure

        Dim dt As New DataTable()
        command.Fill(dt)

        griddocument_attachment.DataSource = dt
        griddocument_attachment.DataBind()

    End Sub

    Function document_attachment_Insert(ByVal filename As String, ByVal fileguid As String) As Integer
        Dim connection As New SqlConnection(GlobalDBConnection)
        Dim command As New SqlCommand("document_attachment_Insert", connection)
        command.CommandType = CommandType.StoredProcedure

        Dim p_attachmentid As New SqlParameter("@attachmentid", SqlDbType.Int)
        p_attachmentid.Direction = ParameterDirection.Output
        command.Parameters.Add(p_attachmentid)
        command.Parameters.AddWithValue("@filename", filename)
        command.Parameters.AddWithValue("@fileguid", fileguid)

        command.Connection.Open()
        Try
            command.ExecuteNonQuery()
        Catch
            Return 0
        Finally
            command.Connection.Close()
        End Try

        If Not IsDBNull(p_attachmentid.Value) Then
            Return CInt(p_attachmentid.Value)
        Else
            Return 0
        End If
    End Function

End Class