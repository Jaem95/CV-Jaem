﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="CV.aspx.vb" Inherits="CV.CV" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
    <head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="CV de José Antonio Escobar Macías ">
    <meta name="keywords" content="Resumen de mi vida como estudiante y  como profesionista">


	<title>JAEM's CV</title>

  <!-- Web Fonts -->
  <link href='http://fonts.googleapis.com/css?family=Roboto:400,300,500,700' rel='stylesheet' type='text/css'>
	<!-- Bootstrap core CSS -->
        <link href="../assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
	<!-- Font Awesome CSS -->
	<link href="../assets/css/font-awesome.min.css" rel="stylesheet" media="screen">
	<!-- Animate css -->
  <link href="../assets/css/animate.css" rel="stylesheet">
  <!-- Magnific css -->
	<link href="../assets/css/magnific-popup.css" rel="stylesheet">
	<!-- Custom styles CSS -->
	<link href="../assets/css/style.css" rel="stylesheet" media="screen">
  <!-- Responsive CSS -->
  <link href="../assets/css/responsive.css" rel="stylesheet">

 

  <link rel="shortcut icon" href="../assets/images/ico/favicon.png">
  <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/images/ico/apple-touch-icon-144-precomposed.png">
  <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/images/ico/apple-touch-icon-114-precomposed.png">
  <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/images/ico/apple-touch-icon-72-precomposed.png">
  <link rel="apple-touch-icon-precomposed" href="../assets/images/ico/apple-touch-icon-57-precomposed.png">

        <style type="text/css">
        .bodyGIF
        {
            margin: 0;
            padding: 0;
            font-family: Arial;
        }
       .modalGIF
        {
            position: fixed;
            z-index: 999;
            height: 100%;
            width: 100%;
            top: 0;
            background-color: Black;
            filter: alpha(opacity=60);
            opacity: 0.6;
            -moz-opacity: 0.8;
        }
        .centerGIF
        {
            z-index: 1000;
            margin: 300px auto;
            padding: 10px;
            width: 130px;
            background-color: White;
            border-radius: 10px;
            filter: alpha(opacity=100);
            opacity: 1;
            -moz-opacity: 1;
        }
        .center2GIF
        {
            height: 128px;
            width: 128px;
        }
    </style>

</head>
<form runat="server">
      <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
                                
    <body>
	<!-- Preloader -->
	<div id="tt-preloader">
		<div id="pre-status">
			<div class="preload-placeholder"></div>
		</div>
	</div>

	<!-- Home Section -->
	<section id="home" class="tt-fullHeight" data-stellar-vertical-offset="50" data-stellar-background-ratio="0.2">
		<div class="intro">
			<div class="intro-sub">I am Jose Antonio Escobar Macias</div>
			<h1><span>Computer </span>Systems <span>Engineer</span></h1>
              <p>I am a fully professional freelance creative User Interface  &amp; Developer<br> Involving with latest web designing , developing tools  and  technologies  <br> Feel free to know about me.</p>

      <div class="social-icons">
        <ul class="list-inline">
          <li><a href="https://www.facebook.com/TonoEscobar95"  target="_blank"><i class="fa fa-facebook"></i></a></li>
          <li><a href="https://twitter.com/Jaem95"  target="_blank"><i class="fa fa-twitter"></i></a></li>
          <li><a href="https://gitlab.com/u/Jaem95" target="_blank"><i class="fa fa-git"></i></a></li>
          <li><a href="https://www.instagram.com/jaem_07/" target="_blank"><i class="fa fa-instagram"></i></a></li>
        </ul>
      </div> <!-- /.social-icons -->
		</div>

		<div class="mouse-icon">
			<div class="wheel"></div>
		</div>
	</section><!-- End Home Section -->




	<!-- Navigation -->
	<header class="header">
		<nav class="navbar navbar-custom" role="navigation">
			<div class="container">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#custom-collapse">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
				</div>

				<div class="collapse navbar-collapse" id="custom-collapse">
					<ul class="nav navbar-nav navbar-right">
						<li><a href="#home">Home</a></li>
						<li><a href="#about">About</a></li>
						<li><a href="#resume">Resume</a></li>
						<li><a href="#skills">Skills</a></li>
						 
            <li><a href="#contact">Contact</a></li>
					</ul>
				</div>
			</div><!-- .container -->
		</nav>
	</header><!-- End Navigation -->


    <!-- About Section -->
    <section id="about" class="about-section section-padding">

        <asp:UpdatePanel runat="server">
            <ContentTemplate>

                 <div class="container">
        <h2 class="section-title wow fadeInUp">About Me</h2>

        <div class="row">

          <div class="col-md-4 col-md-push-8">
            <div class="biography">
              <div class="myphoto">
                <img src="../assets/images/myphoto.jpg" alt="">
              </div>
              <ul>
                <li><strong>Name:</strong> Jose Antonio Escobar Macias</li>
                <li><strong>Date of birth:</strong> 07 Sep 1995</li>
                <!--<li><strong>Address:</strong> Josefa Ortiz de Dominguez 476 Col. Casa Blanca</li>-->
                <li><strong>Nationality:</strong> Mexican</li>
                <li><strong>Phone:</strong> (52) 722 486 9982</li>
                <li><strong>Email:</strong> oso_tolucajaem95@hotmail.com  
                  <br />
                    tooescobar@gmail.com
                     <br /></li>
              </ul>
            </div>
          </div> <!-- col-md-4 -->

          <div class="col-md-8 col-md-pull-4">
            <div class="short-info wow fadeInUp">
              <h3>Objective</h3>
              <p>The main purpose of this project is to let you know in a attractive way the person and the developer that I am. I think that things that you do in your life, are the reason of why people are going to talk about you in the future. You only live once, like job offers too, If you are going to sell your work, you have to do like never before, as if it were the last time you did. I focus on that, by using my interpersonal skills to build good user experience and create an strong interest in my employers. I hope to develop skills over time, become an honest asset to the business. As an individual, I'm self-confident you’ll find me creative, funny and naturally passionate. I’m a forward thinker, which others may find inspiring when working as a team.</div>

            <div class="short-info wow fadeInUp">
              <h3>What I Do ?</h3>
              <p> I´ve been working with many programming languages and different tools. Fortunately  I can say that I can work with  C, C++, Java,C#,Visual Basic.NET,HTML5,JavaScript,CiscoIOS,CSS3,Python ,Transact-SQL,Lisp,PL/SQL,SQL,C# with Unity  and UFT <br>
                   <br />
                   I really appreciate opportunites that life had gave me, because I have learnt many things like  different frameworks of Web Development as ASP.Net  and in the same way Computer  packages as Microsoft Business Intelligence, Oracle and a little bit of SAP . <br>
                  <br />
                   Something that is very important to mention is that I love computers because when a I was kid I learned how to fix them, so I have knowledge about Hardware like the most important components of a computer and the possible causes of why a computer stops working<br>
                   <br />
               </p>

              <ul class="list-check">
                <li>Interface Design</li>
                <li>Product Design</li>
                <li>C and C++ Console Application development</li>
                <li>Web application development</li>
                <li>Networking Configuration </li>
                <li>Use and Develop of Testing Apps</li>
                <li>Photograph and Video  Edition</li>
                <li>Management of Data Bases</li>
                <li>Mini Games development in Unity</li>
                <li>Java App development</li>
                <li>Microsoft SSIS,SSAS,SSRS development</li>
                 <li>Windows Services development</li>
                <li>Diagnostic and Fixing of Computers,Tablets and MobilePhones</li>
              </ul>
            </div>

            <div class="my-signature">
              <img src="../assets/images/sign.png" alt="">
            </div>

            <div class="download-button">
 
              <a class="btn btn-info btn-lg" href="#contact"><i class="fa fa-paper-plane"></i>Send me message</a>
              <!--<a  name="descargarCV"  type="submit" class="btn btn-primary btn-lg" href="#"><i class="fa fa-download"></i>download my cv</a>-->
              
             <%-- <a> <input  name="descargarCV"  type="submit" value="Descarga CV" class="btn btn-primary btn-lg"><i class="fa fa-download"></i> </a>--%>
                 <asp:Button ID="btnDescargaCV" runat="server" Text="Descargar CV" class="btn btn-primary btn-lg" />
            
                 
    
            </div>
          </div>


        </div> <!-- /.row -->
      </div> <!-- /.container -->

            </ContentTemplate>
        </asp:UpdatePanel>
     
    </section>
        <!-- End About Section -->


<!--    <!-- Video Section 
    <section id="video" class="video-section">
      <div class="tf-bg-overlay">

        <div class="container">

          <div class="video-intro">
             <a class="popup-video" href="https://www.youtube.com/watch?v=LGOjcu0HRec"> <i class="fa fa-play"></i>  </a>
             <h2>Video Introducing</h2>
          </div>

        </div>

        <!--/.container
      </div>
      <!--/.overlay
    </section>
    <!-- /.Video Section -->  


    <!-- Resume Section -->
    <section id="resume" class="resume-section section-padding">
        <div class="container">
            <h2 class="section-title wow fadeInUp">Resume</h2>
            <div class="row">
                <div class="col-md-12">
                    <div class="resume-title">
                        <h3>Education</h3>
                    </div>
                    <div class="resume">
                        <ul class="timeline">
                            <li>
                                <div class="posted-date">
                                    <span class="month">2003-Actually</span>
                                </div><!-- /posted-date -->

                                <div class="timeline-panel wow fadeInUp">
                                    <div class="timeline-content">
                                        <div class="timeline-heading">
                                            <h3>University</h3>
                                             <span>Instituto Tecnologico de Estudios Superiores de Monterrey,Toluca , México</span>
                                        </div><!-- /timeline-heading -->

                                        <div class="timeline-body">
                                            <p>Seventh semester of the career and I´m doing my professional practices. </br></p>
                                        </div><!-- /timeline-body -->
                                    </div> <!-- /timeline-content -->
                                </div><!-- /timeline-panel -->
                            </li>

                            <li class="timeline-inverted">
                                <div class="posted-date">
                                    <span class="month">2010-2013</span>
                                </div><!-- /posted-date -->

                                <div class="timeline-panel wow fadeInUp">
                                    <div class="timeline-content">
                                        <div class="timeline-heading">
                                            <h3>High School Certificate</h3>
                                            <span>Instituto Tecnologico de Estudios Superiores de Monterrey,Toluca , México</span>
                                        </div><!-- /timeline-heading -->

                                        <div class="timeline-body">
                                            <p> From this collegue is  the actual style of life that I have, Here is  where I decide to dedicate my life to the Computers and all the things that it has, here I knew  people that nowadays are brothers and sisters to me. <br> During this period I have my first job and I started to develop apps and console tasks.</p>
                                        </div><!-- /timeline-body -->
                                    </div> <!-- /timeline-content -->
                                </div> <!-- /timeline-panel -->
                            </li>

                            <li>
                                <div class="posted-date">
                                  <span class="month">2007-2010</span>
                                </div><!-- /posted-date -->

                                <div class="timeline-panel wow fadeInUp">
                                    <div class="timeline-content">
                                        <div class="timeline-heading">
                                            <h3>Secondary School certificate</h3>
                                            <span>Instituto Simon Bolivar,Toluca,México</span>
                                        </div><!-- /timeline-heading -->

                                        <div class="timeline-body">
                                            <p>This  period of time are one of the most imporant in my life, here is when I start to fall in love with the computers and videogames. </p>
                                        </div><!-- /timeline-body -->
                                    </div> <!-- /timeline-content -->
                                </div><!-- /timeline-panel -->
                            </li>
                            
                            
                            
                             
                            
                        </ul>
                    </div>
                </div>
            </div><!-- /row -->
            
             <div class="row">
                <div class="col-md-12">
                    <div class="resume-title">
                        <h3>Experience</h3>
                    </div>
                    <div class="resume">
                        <ul class="timeline">
                            <li class="timeline-inverted">
                                <div class="posted-date">
                                  <span class="month"> February 2016-Actually</span>
                                </div><!-- /posted-date -->

                                <div class="timeline-panel wow fadeInUp">
                                    <div class="timeline-content">
                                        <div class="timeline-heading">
                                            <h3>Developer</h3>
                                            <span>Corporativo La Moderna</span>
                                        </div><!-- /timeline-heading -->

                                        <div class="timeline-body">
                                            <p>My lovely and actual job, this is my first job using my abilities of engineer, I am working at Corporativo La Moderna as developer of web applications, web services, SQL Jobs and Stored Procedures. 
This is one of the most important steps of my life, here I discover that the things that we learned at the school are nothing to the real word, in a very particular way I can say that the best thing that the school is teaching me is the logic, at the begin of the year I didn´t know what as an ASP.Net pages, a SSIS Package, a IIS Configuration, I´m learning a lot of things that I never thought that existed</p>
                                        </div><!-- /timeline-body -->
                                    </div> <!-- /timeline-content -->
                                </div> <!-- /timeline-panel -->
                            </li>

                            <li>
                                <div class="posted-date">
                                  <span class="month"> December 2014 - February 2016</span>
                                </div><!-- /posted-date -->

                                <div class="timeline-panel wow fadeInUp">
                                    <div class="timeline-content">
                                        <div class="timeline-heading">
                                            <h3>Techincian</h3>
                                            <span>INTAP</span>
                                        </div><!-- /timeline-heading -->

                                        <div class="timeline-body">
                                            <p>This was my last job as technician, when I left  Xinantek,   I considered to look a job  that has relation with my career, unfortunatly I was  in 5th semester and I wasn't enough ready for the real world. At the same time one friend of Xinantek was opening his own business, so he invited me to be part of his team, I accept  to have a  extra entry of money.
                                            </br> Even though that this job was not in my scheduler, I consider that this job was the door to  know people, to develop my interpersonal habilities and most imporant, to lose the fear of talk and make business with the people.</p>
                                        </div><!-- /timeline-body -->
                                    </div> <!-- /timeline-content -->
                                </div><!-- /timeline-panel -->
                            </li>
                            
                            <li class="timeline-inverted">
                                <div class="posted-date">
                                  <span class="month">May 2014 - September 2014</span>
                                </div><!-- /posted-date -->

                                <div class="timeline-panel wow fadeInUp">
                                    <div class="timeline-content">
                                        <div class="timeline-heading">
                                            <h3>Technician</h3>
                                            <span>Xinantek</span>
                                        </div><!-- /timeline-heading -->

                                        <div class="timeline-body">
                                            <p>This was my second job, this job was with the school that tought me to fix computers, they have his own business in "Plaza de la Tecnologia" in the center of Toluca,like in Impercom, my labor here was to fix computers, but an extra task that I have was the direct sell and attention to the customers, I was in the main local,and the customers going there to buy supplies and to pick the computers. 
For last here I learned how to fix tablets and cell phones. 
I consider that here I finished my formation as Technician, I learnt all the the things that I can and actually I continue updating myself about new technologies and fails that the actuals computers have.</p>
                                        </div><!-- /timeline-body -->
                                    </div> <!-- /timeline-content -->
                                </div> <!-- /timeline-panel -->
                            </li>
                            
                            <li>
                                <div class="posted-date">
                                  <span class="month">May 2013- August 2013</span>
                                </div><!-- /posted-date -->

                                <div class="timeline-panel wow fadeInUp">
                                    <div class="timeline-content">
                                        <div class="timeline-heading">
                                            <h3>Technician</h3>
                                            <span>Impercom</span>
                                        </div><!-- /timeline-heading -->

                                        <div class="timeline-body">
                                            <p>After that I took a course of computers fixing,I got my first Job in Impercom, this is a little enterprise in Toluca, this enterprise focuses on the distribution and fixing of computers and its componentes, my labor here was to fix computers and printers.</p>
                                        </div><!-- /timeline-body -->
                                    </div> <!-- /timeline-content -->
                                </div><!-- /timeline-panel -->
                            </li>
                            
                        </ul>
                    </div>
                </div>
            </div><!-- /row -->


           <!-- EXPERIENCIA -->
        </div><!-- /.container -->
    </section><!-- End Resume Section -->
    
 <%--   <!-- Skills Section -->
    <section id="presentacion" class="skills-section section-padding">
      <div class="container">
        <h2 class="section-title wow fadeInUp">Presentation</h2>
            <iframe src="https://app.emaze.com/@AZFCOWWF/presentation-name" width="100%" height="540px" ></iframe>
      </div><!-- /.container -->
    </section><!-- End Skills Section -->--%>


         <!-- Skills Section -->
    <section id="skills" class="skills-section section-padding">
      <div class="container">
        <h2 class="section-title wow fadeInUp">Programming Skills</h2>

        <div class="row">
          <div class="col-md-6">
            <div class="skill-progress">
              <div class="skill-title"><h3>HTML5  &amp; CSS3 </h3></div> 
              <div class="progress">
                <div class="progress-bar six-sec-ease-in-out" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" ><span>75%</span>
                </div>
              </div><!-- /.progress -->
            </div><!-- /.skill-progress -->

            <div class="skill-progress">
              <div class="skill-title"><h3>Visual Basic</h3></div> 
              <div class="progress">
                <div class="progress-bar six-sec-ease-in-out" role="progressbar" aria-valuenow="55" aria-valuemin="0" aria-valuemax="100" ><span>55%</span>
                </div>
              </div><!-- /.progress -->
            </div><!-- /.skill-progress -->
            <div class="skill-progress">
              <div class="skill-title"><h3>C++ &amp; C </h3></div>  
              <div class="progress">
                <div class="progress-bar six-sec-ease-in-out" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" ><span>50%</span>
                </div>
              </div><!-- /.progress -->
            </div><!-- /.skill-progress -->
             <div class="skill-progress">
              <div class="skill-title"><h3>Java</h3></div> 
              <div class="progress">
                <div class="progress-bar six-sec-ease-in-out" role="progressbar" aria-valuenow="67" aria-valuemin="0" aria-valuemax="100" ><span>67%</span>
                </div>
              </div><!-- /.progress -->
            </div><!-- /.skill-progress -->
          </div><!-- /.col-md-6 -->

          <div class="col-md-6">
           
            <div class="skill-progress">
              <div class="skill-title"><h3>SQL/MySQL </br>/OracleForms/PLSQL</h3></div> 
              <div class="progress">
                <div class="progress-bar six-sec-ease-in-out" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" ><span>75%</span>
                </div>
              </div><!-- /.progress -->
            </div><!-- /.skill-progress -->
            </br>
            <div class="skill-progress">
              <div class="skill-title"><h3>Informix 4G</h3></div>  
              <div class="progress">
                <div class="progress-bar six-sec-ease-in-out" role="progressbar" aria-valuenow="35" aria-valuemin="0" aria-valuemax="100" ><span>35%</span>
                </div>
              </div><!-- /.progress -->
            </div><!-- /.skill-progress -->
             <div class="skill-progress">
              <div class="skill-title"><h3>JavaScript</br>Serverlts</h3></div>  
              <div class="progress">
                <div class="progress-bar six-sec-ease-in-out" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" ><span>45%</span>
                </div>
              </div><!-- /.progress -->
            </div><!-- /.skill-progress -->
          </div><!-- /.col-md-6 -->
        </div><!-- /.row -->
        
        <h2 class="section-title wow fadeInUp">Graphic Skills</h2>
         <div class="row">
          <div class="col-md-6">
            <div class="skill-progress">
              <div class="skill-title"><h3>Photoshop</h3></div> 
              <div class="progress">
                <div class="progress-bar six-sec-ease-in-out" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" ><span>45%</span>
                </div>
              </div><!-- /.progress -->
            </div><!-- /.skill-progress -->

            <div class="skill-progress">
              <div class="skill-title"><h3>iMovie</h3></div> 
              <div class="progress">
                <div class="progress-bar six-sec-ease-in-out" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" ><span>70%</span>
                </div>
              </div><!-- /.progress -->
            </div><!-- /.skill-progress -->
          </div><!-- /.col-md-6 -->

          <div class="col-md-6">
           
           
            <div class="skill-progress">
              <div class="skill-title"><h3>Paint.NET</h3></div>  
              <div class="progress">
                <div class="progress-bar six-sec-ease-in-out" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" ><span>50%</span>
                </div>
              </div><!-- /.progress -->
            </div><!-- /.skill-progress -->
          </div><!-- /.col-md-6 -->
            </div><!-- /.row -->

          
            <h2 class="section-title wow fadeInUp">Computers Packages</h2>
         <div class="row">
         
          <div class="col-md-6">
            <div class="skill-progress">
              <div class="skill-title"><h3>Microsoft Bussines </br>  Intelligence</h3></div> 
              <div class="progress">
                <div class="progress-bar six-sec-ease-in-out" role="progressbar" aria-valuenow="55" aria-valuemin="0" aria-valuemax="100" ><span>55%</span>
                </div>
              </div><!-- /.progress -->
            </div><!-- /.skill-progress -->
            </br>
            <div class="skill-progress">
              <div class="skill-title"><h3>Office/Visio </br>  Sharepoint</h3></div> 
              <div class="progress">
                <div class="progress-bar six-sec-ease-in-out" role="progressbar" aria-valuenow="72" aria-valuemin="0" aria-valuemax="100" ><span>72%</span>
                </div>
              </div><!-- /.progress -->
            </div><!-- /.skill-progress -->
                 </div><!-- /.col-md-6 -->
          
          

          <div class="col-md-6">
            <div class="skill-progress">
              <div class="skill-title"><h3>SAP</h3></div> 
              <div class="progress">
                <div class="progress-bar six-sec-ease-in-out" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" ><span>20%</span>
                </div>
              </div><!-- /.progress -->
            </div><!-- /.skill-progress -->
            <div class="skill-progress">
              <div class="skill-title"><h3>Oracle</h3></div> 
              <div class="progress">
                <div class="progress-bar six-sec-ease-in-out" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" ><span>50%</span>
                </div>
              </div><!-- /.progress -->
            </div><!-- /.skill-progress -->
                 </div><!-- /.col-md-6 -->
            </div><!-- /.row -->

          
          <h2 class="section-title wow fadeInUp">Game Skills</h2>
         <div class="row">
          <div class="col-md-6">
            <div class="skill-progress">
              <div class="skill-title"><h3>Unity</h3></div> 
              <div class="progress">
                <div class="progress-bar six-sec-ease-in-out" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" ><span>50%</span>
                </div>
              </div><!-- /.progress -->
            </div><!-- /.skill-progress -->

       
          </div><!-- /.col-md-6 -->

          <div class="col-md-6">
            <div class="skill-progress">
              <div class="skill-title"><h3>Game Maker</h3></div> 
              <div class="progress">
                <div class="progress-bar six-sec-ease-in-out" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" ><span>60%</span>
                </div>
              </div><!-- /.progress -->
            </div><!-- /.skill-progress -->
            
            
          </div><!-- /.col-md-6 -->
            </div><!-- /.row -->

            
          <h2 class="section-title wow fadeInUp">Mobile Applications Development </h2>
         <div class="row">
          <div class="col-md-6">
            <div class="skill-progress">
              <div class="skill-title"><h3>iOS Apps</h3></div> 
              <div class="progress">
                <div class="progress-bar six-sec-ease-in-out" role="progressbar" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100" ><span>25%</span>
                </div>
              </div><!-- /.progress -->
            </div><!-- /.skill-progress -->

       
          </div><!-- /.col-md-6 -->

          <div class="col-md-6">
            <div class="skill-progress">
              <div class="skill-title"><h3>Android Apps</h3></div> 
              <div class="progress">
                <div class="progress-bar six-sec-ease-in-out" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" ><span>45%</span>
                </div>
              </div><!-- /.progress -->
            </div><!-- /.skill-progress -->
            
            
          </div><!-- /.col-md-6 -->
            </div><!-- /.row -->

          
           <h2 class="section-title wow fadeInUp">Networking Skills</h2>
         <div class="row">
          <div class="col-md-6">
            <div class="skill-progress">
              <div class="skill-title"><h3>Cisco IOS</h3></div> 
              <div class="progress">
                <div class="progress-bar six-sec-ease-in-out" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" ><span>60%</span>
                </div>
              </div><!-- /.progress -->
            </div><!-- /.skill-progress -->

         
          </div><!-- /.col-md-6 -->

          <div class="col-md-6">
            <div class="skill-progress">
              <div class="skill-title"><h3>Switching and Routing</h3></div> 
              <div class="progress">
                <div class="progress-bar six-sec-ease-in-out" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" ><span>45%</span>
                </div>
              </div><!-- /.progress -->
            </div><!-- /.skill-progress -->
          
            
          </div><!-- /.col-md-6 -->
            </div><!-- /.row -->

          
          <h2 class="section-title wow fadeInUp">Technician Skills</h2>
         <div class="row">
          <div class="col-md-6">
            <div class="skill-progress">
              <div class="skill-title"><h3>Diagnostic  and Fix of Computers</h3></div> 
              <div class="progress">
                <div class="progress-bar six-sec-ease-in-out" role="progressbar" aria-valuenow="95" aria-valuemin="0" aria-valuemax="100" ><span>95%</span>
                </div>
              </div><!-- /.progress -->
            </div><!-- /.skill-progress -->
 </br> 
            <div class="skill-progress">
              <div class="skill-title"><h3> Diagnostic and Fix of Mobile Phones</h3></div> 
              <div class="progress">
                <div class="progress-bar six-sec-ease-in-out" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" ><span>80%</span>
                </div>
              </div><!-- /.progress -->
            </div><!-- /.skill-progress -->
           
          </div><!-- /.col-md-6 -->

          <div class="col-md-6">
            <div class="skill-progress">
              <div class="skill-title"><h3>Diagnostic and Fix of Tablets</h3></div>  
              <div class="progress">
                <div class="progress-bar six-sec-ease-in-out" role="progressbar" aria-valuenow="85" aria-valuemin="0" aria-valuemax="100" ><span>85%</span>
                </div>
              </div><!-- /.progress -->
            </div><!-- /.skill-progress -->
            </br> 
            <div class="skill-progress">
              <div class="skill-title"><h3>Instalation and Configuration of Software</h3></div> 
              <div class="progress">
                <div class="progress-bar six-sec-ease-in-out" role="progressbar" aria-valuenow="95" aria-valuemin="0" aria-valuemax="100" ><span>95%</span>
                </div>
              </div><!-- /.progress -->
            </div><!-- /.skill-progress -->
           
          </div><!-- /.col-md-6 -->
        </div><!-- /.row -->

        <div class="skill-chart text-center">
          <h3>More skills</h3>
        </div>
          
        <div class="row more-skill text-center col-lg-offset-1" align="center">
          <div class="col-xs-12 col-sm-4 col-md-2">
              <div class="chart" data-percent="85" data-color="e74c3c">
                    <span class="percent"></span>
                    <div class="chart-text">
                      <span>leadership</span>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-4 col-md-2">
              <div class="chart" data-percent="60" data-color="2ecc71">
                    <span class="percent"></span>
                    <div class="chart-text">
                      <span>Creativity</span>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-4 col-md-2">
              <div class="chart" data-percent="85" data-color="3498db">
                    <span class="percent"></span>
                    <div class="chart-text">
                      <span>Management</span>
                    </div>
                </div>
            </div>
          
            <div class="col-xs-12 col-sm-4 col-md-2">
              <div class="chart" data-percent="68" data-color="3498db">
                    <span class="percent"></span>
                    <div class="chart-text">
                      <span>Marketing</span>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-4 col-md-2">
              <div class="chart" data-percent="85" data-color="3498db">
                    <span class="percent"></span>
                    <div class="chart-text">
                      <span>Motivation</span>
                    </div>
                </div>
            </div>
        </div>

      </div><!-- /.container -->
    </section><!-- End Skills Section -->
        

     <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
                                <ProgressTemplate>
                                    <div class="modalGIF">
                                        <div class="centerGIF">
                                            <%--<asp:Image alt="" ImageUrl="~/images/loader.gif" runat="server" />--%>
                                            <img alt="" src="http://www.aspsnippets.com/Demos/loader4.gif" />
                                        </div>
                                    </div>
                                </ProgressTemplate>
                              </asp:UpdateProgress>
    <!-- Contact Section -->
    <section id="contact" class="contact-section section-padding">
      <div class="container">
        <h2 class="section-title wow fadeInUp">Get in touch</h2>
           

        <div class="row">
          <div class="col-md-6">
            <div class="contact-form">
              <strong>Send me a message</strong>
           
                
                        
                           


                             <asp:UpdatePanel runat="server" ID="UpdatePanel1">
                              <ContentTemplate>
                                    <div class="form-group">
                                      <label for="name">Name</label>
                                        <asp:TextBox ID="txtName" CssClass="form-control" runat="server"></asp:TextBox>
                                    </div>

                                    <div class="form-group">
                                      <label for="email">Email</label>
                                      <asp:TextBox ID="txtEmail" CssClass="form-control" runat="server"></asp:TextBox>
                                    </div>

                                    <div class="form-group">
                                      <label for="subject">Subject</label>
                                     <asp:TextBox ID="txtSubject" CssClass="form-control" runat="server"></asp:TextBox>
                                    </div>

                                    <div class="form-group">
                                      <label for="message">Message</label>
                                     <asp:TextBox ID="txtMessage" CssClass="form-control" runat="server" Rows="5" TextMode="MultiLine"></asp:TextBox>
                                    </div>
                                    <div class="form-group">
                                       <h4> <asp:Label ID="lblError" runat="server" Text="" class="label label-pill label-info" ></asp:Label> </h4> 
                                    </div>

                                    <asp:Button ID="btnEnviarCorreo" runat="server" Text="Send Email"  class="btn btn-primary" />
                                </ContentTemplate>
                        </asp:UpdatePanel>
                
              
            </div><!-- /.contact-form -->
          </div><!-- /.col-md-6 -->

          <div class="col-md-6">
            <div class="row center-xs">
             

              <div class="col-sm-6">
                <i class="fa fa-mobile"></i>
                <div class="contact-number">
                  <strong>Phone Number 722 486 9982<br>
                </div>
              </div>
            </div>

          <div class="row">
            <div class="col-sm-12">
              <div class="location-map">
       <!--    /*     <div id="mapCanvas" class="map-canvas"></div>*/-->
                
               
               <iframe  id="mapCanvas" class="map-canvas" src="https://www.google.com/maps/embed?pb=!1m10!1m8!1m3!1d470.73760833544264!2d-99.60154408576415!3d19.286676770369446!3m2!1i1024!2i768!4f13.1!5e0!3m2!1ses-419!2smx!4v1465013987375" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
                
              </div>
            </div>
          </div>

          </div>
        </div><!-- /.row -->
      </div><!-- /.container -->
    </section><!-- End Contact Section -->


	<!-- Footer Section -->
    <footer class="footer-wrapper">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <div class="copyright text-center">
              <p>&copy; Jose Antonio Escobar Macias. All rights reserved.</p>
            </div>
          </div>
        </div>
      </div>
    </footer><!-- End Footer Section -->


	<!-- Scroll-up -->
	<div class="scroll-up">
		<a href="#home"><i class="fa fa-angle-up"></i></a>
	</div>

	<!-- Javascript files -->
	<script src="../assets/js/jquery.js"></script>
	<script src="../assets/bootstrap/js/bootstrap.min.js"></script>
  <script src="../assets/js/jquery.stellar.min.js"></script>
	<script src="../assets/js/jquery.sticky.js"></script>
  <script src="../assets/js/smoothscroll.js"></script>
	<script src="../assets/js/wow.min.js"></script>
  <script src="../assets/js/jquery.countTo.js"></script>
  <script src="../assets/js/jquery.inview.min.js"></script> 
  <script src="../assets/js/jquery.easypiechart.js"></script>
  <script src="../assets/js/jquery.shuffle.min.js"></script>
  <script src="../assets/js/jquery.magnific-popup.min.js"></script>
  <script src="http://a.vimeocdn.com/js/froogaloop2.min.js"></script>
  <script src="../assets/js/jquery.fitvids.js"></script>
  <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&amp;sensor=false"></script>
  <script src="../assets/js/scripts.js"></script>
</body>


</form>



</html>
