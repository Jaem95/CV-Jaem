﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Uploader.aspx.vb" Inherits="CV.Uploader" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
            <%--<asp:Content ID="Content1" ContentPlaceHolderID="scripts" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Body" runat="server">--%>
    <div style="width: 350px; background-color: #f2ceea; padding: 10px; margin: 20px">
        Seleccione archivo:<asp:FileUpload ID="FileUpload1" runat="server" />
        <hr />
        <asp:Button ID="btnUpload" runat="server" Text="Guardar archivo" />
    </div>
    <asp:GridView ID="griddocument_attachment" BackColor="lightgreen" Width="400px" runat="server"
        AutoGenerateColumns="False" DataKeyNames="fileguid" HeaderStyle-HorizontalAlign="Left"
        HeaderStyle-VerticalAlign="Top" RowStyle-VerticalAlign="Top">
        <Columns>
            <asp:TemplateField HeaderStyle-Width="3%">
                <ItemTemplate>
                    <asp:Image ID="Image1" ImageUrl="~/images/file.png" runat="server" />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Archivo">
                <ItemTemplate>
                    <%# Eval("filename")%>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Descargar" HeaderStyle-Width="5%">
                <ItemTemplate>
                    <asp:HyperLink ID="hplAttachment" Text='<%# Eval("filename")%>' NavigateUrl='<%# String.Format("~/Pages/Downloader.aspx?guid={0}", Eval("fileguid")) %>'
                        ImageUrl="~/images/download.png" runat="server"></asp:HyperLink>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
<%--</asp:Content>--%>

    
    </div>
    </form>
</body>
</html>
